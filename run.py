import threading

from config import TEMPLATES_FOLDER, STATIC_FOLDER
from oshipka import init_db

from populate import populate_db
from webapp.app import app, video_loop

if init_db(app):
    populate_db(app)

app.config['JSON_AS_ASCII'] = False
app.template_folder = TEMPLATES_FOLDER
app.static_folder = STATIC_FOLDER

if __name__ == "__main__":
    t = threading.Thread(target=video_loop, args=())
    t.start()
    app.run(threaded=True)
    t.join()
