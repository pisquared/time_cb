import csv

import json

import queue
import random
import subprocess

import os
import re
from datetime import datetime, timedelta
from flask import render_template, Response, request, jsonify
from time import sleep

from config import basepath
from oshipka.persistance import db
from oshipka.webapp import app
from webapp.models import Feedback

delta_idx = 1
min_minutes = 20
max_minutes = 30

DELTA_SYNC = 0  # load the video this many seconds before switching
total_eps = 30
CB_PATH = "/home/pi2/cb/podcasts_bg/bg{:02d}.mp4"


def get_now():
    return datetime.now().strftime("%Y-%m-%dT%H:%M:%S")


def sleep_until(interval_duration):
    return (datetime.now() + timedelta(seconds=interval_duration)).strftime("%Y-%m-%dT%H:%M:%S")


def fmt_sec(sec):
    h = 0
    if sec >= 3600:
        h, sec = sec // 3600, sec % 3600
    m, s = sec // 60, sec % 60
    if h:
        return "{}:{:02d}:{:02d}".format(h, m, s)
    return "{:02d}:{:02d}".format(m, s)


def fmt_gt(gt):
    return gt - delta_idx + 1


class Video(object):
    def __init__(self):
        self.path = ""
        self.current_id = "01"
        self.duration = 0
        self.current_time = 0

        self.start_time = 0
        self.end_time = 0

    def get_video_duration(self):
        result = subprocess.run(["ffprobe", "-v", "error", "-show_entries",
                                 "format=duration", "-of",
                                 "default=noprint_wrappers=1:nokey=1", self.path],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        return int(float(result.stdout))

    def goto(self, video_id):
        self.current_id = video_id
        self.path = CB_PATH.format(video_id)
        self.duration = self.get_video_duration()

    def seek(self, seek_to, end_time):
        self.current_time = seek_to
        self.end_time = end_time

    def details(self):
        return VIDEO_DETAILS.get("bg{:02d}".format(self.current_id), {})


VIDEO = Video()
VIDEO_DETAILS = dict()


def video_loop():
    with open(os.path.join(basepath, "save_data", 'video_details.csv')) as f:
        reader = csv.DictReader(f)
        for row in reader:
            VIDEO_DETAILS[row["episode_id"]] = row
    while True:
        gt = random.randint(0 + delta_idx, total_eps - 1 + delta_idx)
        VIDEO.goto(gt)
        interval_duration = random.randint(min_minutes, max_minutes)
        seek_to = random.randint(0, VIDEO.duration - interval_duration)
        end_time = seek_to + interval_duration
        VIDEO.seek(seek_to, end_time)

        play_from, play_until = fmt_sec(seek_to), fmt_sec(end_time)
        play_dur = fmt_sec(interval_duration)
        print("{}: Playing {} -> {} - {} ({})".format(get_now(), fmt_gt(gt), play_from, play_until, play_dur))
        print("{}: Sleeping until {}".format(get_now(), sleep_until(interval_duration)))
        with open('played.csv', 'a') as f:
            f.write("{},{},{},{}\n".format(get_now(), fmt_gt(gt), play_from, play_until, play_dur))
        while True:
            VIDEO.current_time += 1
            if VIDEO.current_time >= VIDEO.end_time - DELTA_SYNC:
                break
            sleep(1)


@app.after_request
def after_request(response):
    response.headers.add('Accept-Ranges', 'bytes')
    return response


def get_chunk(video_id, byte1=None, byte2=None):
    full_path = CB_PATH.format(video_id)
    file_size = os.stat(full_path).st_size
    start = 1
    length = 102400

    if byte1 < file_size:
        start = byte1
    if byte2:
        length = byte2 + 1 - byte1
    else:
        length = file_size - start

    with open(full_path, 'rb') as f:
        f.seek(start)
        chunk = f.read(length)
    return chunk, start, length, file_size


def update_timing(rating):
    constant_time, percent = 10, 0.3
    remaining_time = VIDEO.end_time - VIDEO.current_time
    delta = int(constant_time + remaining_time * percent)
    if rating == "-1":
        VIDEO.end_time -= delta
    elif rating == "1":
        max_remaining_time = VIDEO.duration - VIDEO.current_time
        if delta > max_remaining_time:
            delta = max_remaining_time
        VIDEO.end_time += delta
    return delta


@app.route('/')
def home():
    return render_template("home.html")


@app.route('/videos/<int:video_id>')
def get_file(video_id):
    range_header = request.headers.get('Range', None)
    byte1, byte2 = 0, None
    if range_header:
        match = re.search(r'(\d+)-(\d*)', range_header)
        groups = match.groups()

        if groups[0]:
            byte1 = int(groups[0])
        if groups[1]:
            byte2 = int(groups[1])

    chunk, start, length, file_size = get_chunk(video_id, byte1, byte2)
    resp = Response(chunk, 206, mimetype='video/mp4', content_type='video/mp4', direct_passthrough=True)
    resp.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(start, start + length - 1, file_size))
    return resp


@app.route('/videos/current/details')
def video_current_details():
    rv = {
        "video_id": VIDEO.current_id,
        "current_time": VIDEO.current_time,
        "end_time": VIDEO.end_time,
        "remaining_time": VIDEO.end_time - VIDEO.current_time,
    }
    rv.update(VIDEO.details())
    return jsonify(rv)


@app.route('/feedback', methods=["post"])
def give_feedback():
    rv = {}
    comment = request.form.get("comment")
    rating = request.form.get("rating")
    video_id = request.form.get("video_id")
    timestamp = request.form.get("timestamp")
    now = datetime.utcnow()
    rv = dict(
        comment=comment,
        rating=rating,
        video_id=video_id,
        timestamp=timestamp,
        created_dt=str(now),
    )
    feedback = Feedback(**rv)
    db.session.add(feedback)
    db.session.commit()
    if rating:
        rv["update_delta"] = update_timing(rating)
    return jsonify(rv)
