from oshipka.persistance import db, ModelController


class Feedback(db.Model, ModelController):
    comment = db.Column(db.Unicode)
    rating = db.Column(db.Unicode)
    video_id = db.Column(db.Unicode)
    timestamp = db.Column(db.Unicode)
    created_dt = db.Column(db.Unicode)
